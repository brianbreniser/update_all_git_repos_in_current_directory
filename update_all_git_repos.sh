#!/bin/bash
set -e # fail on any one command returning non-zero exit code

CUR_DIR=$(pwd)

echo "Pulling git latest changes from all repos";
echo "";

# Find all places with a .git, and get the directory it is in
# Only works for flat structures, it cannot find deeply nested git repos
for i in $(find . -name ".git" | cut -d "/" -f 2); do
    echo "##########################################################";
    echo "Going to: $i";
    echo "##########################################################";
    cd "$i";
    git pull
    cd "$CUR_DIR"
done

echo "";
echo "Complte";
